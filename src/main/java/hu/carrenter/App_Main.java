package hu.carrenter;

import hu.carrenter.db.DbConnect;
import hu.carrenter.db.dao.DaoBerlo;
import hu.carrenter.db.dao.DaoCars;
import hu.carrenter.db.dao.DaoTulajdonos;
import hu.carrenter.db.entity.Berlo;
import hu.carrenter.db.entity.Cars;
import hu.carrenter.db.entity.Tulajdonos;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;


/**
 * TODO: Legyen egy Személy ősosztály, melyből származik egy Bérlő, és egy Tulajdonos tábla is.
 * A Személynek van (id,vezetéknév, keresztnév,telefonszám, emailcím, cím) attribútumai.
 * A bérlőnek ezen kívül van egy felhasználható keretösszege.
 * A Tulajdonosnak pedig egy rating mezője, melyben értékelni lehet az általa kölcsönzésre kínált szolgáltatást.
 * A Cars tábla legyen kiegészítve ezen két entitásra való hivatkozással, és legyen továbbá benne egy kölcsönözhető attribútum,
 * mely értéke attól függ, hogy aktuálisan kölcsönzésre van-e kínálva a jármű. Továbbá legyen benne egy kölcsönzési összeg is, mely a kölcsönzés díját adja meg.
 */
public final class App_Main {

    private static final Logger logger = Logger.getLogger(App_Main.class.getName());

    public static void main(String[] args) throws SQLException {

//       new DbConnect("root", "hummer", "adat", DbConnect.DbType.MARIADB) {
     /* new DbConnect() {
            @Override
            public void execute(Connection connection) throws SQLException {
                DaoBerlo daoBerlo = new DaoBerlo(connection);
                daoBerlo.createTable();

                DaoTulajdonos daoTulajdonos = new DaoTulajdonos(connection);
                daoTulajdonos.createTable();

                DaoCars daoCars = new DaoCars(connection);
                daoCars.createTable();

                daoBerlo.insert(rent);
                daoTulajdonos.insert(alcapone);

                daoCars.insert(mustang);
                daoCars.insert(lada);

          daoCars.getByRendszam("bos-002");

               List<Cars> autok = daoCars.getAll();
              logger.info("masodik auto szine : " + autok.get(1).getSzin());
            }
        }; */
    }
}


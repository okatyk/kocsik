package hu.carrenter.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * @author Barnabas Gnam
 * @since 2017-06-27
 */
public abstract class DbConnect {

    public DbConnect() {
        this("sa", "", "carrenter", DbType.H2);
    }

    public DbConnect(String user, String pass, String db, DbType dbtype) {
        try (Connection connection = DriverManager.getConnection(getUrl(db, dbtype), user, pass)) {
            execute(connection);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String getUrl(String db, DbType dbtype) {
        switch (dbtype) {
            case H2: {
                return "jdbc:h2:~/" + db;
            }

            case MARIADB: {
                return "jdbc:mariadb://localhost:3306/" + db;
            }

            default:
                throw new IllegalArgumentException("Nem megfelelő a DbType");
        }
    }

    public abstract void execute(Connection connection) throws SQLException;

    public enum DbType {
        MARIADB,
        H2,;
    }
}

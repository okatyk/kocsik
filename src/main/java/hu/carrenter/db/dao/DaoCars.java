package hu.carrenter.db.dao;

import hu.carrenter.db.entity.Cars;

import java.sql.*;
import java.util.logging.*;

import static javax.swing.UIManager.getBoolean;
import static javax.swing.UIManager.getInt;


public class DaoCars extends DaoCarRenter implements IDaoCars {

    // TODO: Ezek miért nem public-ok? Miért nem használod őket a tesztekben is?
    public static final String TABLENAME = "cars";
    public static final String EVJARAT = "evjarat";
    public static final String SZIN = "szin";
    public static final String RENDSZAM = "rendszam";
    public static final String ELSOTULAJ = "elsotulaj";
    public static final String TULAJID = "tulaj";
    public static final String BERLOID = "berlo";
    public static final String OSSZEG = "osszeg";
    public static final String ID = "id";

    private int COLUMN_INDEX_TULAJDONOS_ID = 5;
    private int COLUMN_INDEX_BERLO_ID = 6;
    private static final Logger logger = Logger.getLogger(DaoCars.class.getName());

    public DaoCars(Connection connection) throws SQLException {
        mConnection = connection;

    }

 /*   public Cars getByRendszam(String rendszam) throws SQLException {
        Cars ujkocsi = null;
        try (Statement stmt = mConnection.createStatement()) {


            ResultSet resultSet = stmt.executeQuery("SELECT * from " + TABLENAME + " WHERE " + RENDSZAM + "= '" + rendszam + "';");


            DaoTulajdonos daoTulajdonos = new DaoTulajdonos(mConnection);

            resultSet.next();
            Tulajdonos ujtulaj = new Tulajdonos(daoTulajdonos.getById(resultSet.getInt(COLUMN_INDEX_TULAJDONOS_ID)));
            DaoBerlo berlosearch = new DaoBerlo(mConnection);
            Berlo ujberlo = berlosearch.getById(resultSet.getInt(COLUMN_INDEX_BERLO_ID));
            ujkocsi = new Cars(resultSet.getString(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getBoolean(4), ujtulaj, ujberlo, resultSet.getInt(7));
            logger.info(" a kocsi évjárata : " + ujkocsi.getEvjarat());
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            // TODO: Legyen minden catch ágban egy egy exception dobás ilyen ->
            throw new RuntimeException(e);
        }

        return ujkocsi;

    }
*/

  /*  public List<Cars> getAll() throws SQLException {
        List<Cars> lista = new ArrayList<>();
        int i = 0;
        try (Statement stmt = mConnection.createStatement()) {
            ResultSet resultSet = stmt.executeQuery("SELECT * from " + TABLENAME + " ;");
            DaoTulajdonos daoTulajdonos = new DaoTulajdonos(mConnection);
            DaoBerlo berlosearch = new DaoBerlo(mConnection);

            while (resultSet.next()) {

                Tulajdonos ujtulaj = new Tulajdonos(daoTulajdonos.getById(resultSet.getInt(COLUMN_INDEX_TULAJDONOS_ID)));
                Berlo ujberlo = berlosearch.getById(resultSet.getInt(COLUMN_INDEX_BERLO_ID));
                Cars ujkocsi = new Cars(resultSet.getString(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getBoolean(4), ujtulaj, ujberlo, resultSet.getInt(7));
                lista.add(i, ujkocsi);
                i++;
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        }

        return lista;
    }*/


    public void printTable() throws SQLException {
        logger.info("printTableProba");
        try (Statement stmt = mConnection.createStatement()) {


            ResultSet resultSet = stmt.executeQuery("SELECT * from " + TABLENAME + ";");

            while (resultSet.next()) {
                logger.info("rendszam :" + resultSet.getString(1) + " evjarat : " + resultSet.getInt(2) + " szin : " + resultSet.getString(3));
            }


            resultSet.close();
        } catch (
                SQLException e)

        {
            e.printStackTrace();
            throw new RuntimeException(e);

        }

    }


    public void createTable() throws SQLException {
        try (Statement stmt = mConnection.createStatement()) {

            String drop = "DROP TABLE IF EXISTS " + TABLENAME + " ;";

            stmt.executeUpdate(drop);

            String sql = "CREATE TABLE " + TABLENAME
                    + " " + "(" + RENDSZAM + " varchar(7) not NULL PRIMARY KEY , "
                    + " " + EVJARAT + " integer not NULL, "
                    + " " + SZIN + " varchar(255), "
                    + " " + ELSOTULAJ + " TINYINT(1), "
                    + " " + TULAJID + " integer not NULL, "
                    + " " + BERLOID + " integer not NULL, "
                    + " " + OSSZEG + " integer not NULL) "
                 //   + " " + ID + " INT not NULL auto_increment PRIMARY KEY) "
                    + ";";

            stmt.executeUpdate(sql);
            logger.info(TABLENAME + " tabla letrehozva");

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        }
    }

    public void insert(String rendszam, int evjarat, String szin, Boolean elsotulaj, int tulaj, int berlo, int osszeg) throws SQLException {

        String sql = "INSERT INTO " + TABLENAME + " VALUES (?,?,?,?,?,?,?);";
        try (PreparedStatement stmt = mConnection.prepareStatement(sql)) {
            stmt.setString(1, rendszam);
            stmt.setInt(2, evjarat);
            stmt.setString(3, szin);
            stmt.setBoolean(4, elsotulaj);
            stmt.setInt(5, tulaj);
            stmt.setInt(6, berlo);
            stmt.setInt(7, osszeg);
         //   stmt.setInt(8,1);
            stmt.executeUpdate();
            logger.info(TABLENAME + " insert kesz");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        }
    }


   public void insert(Cars auto) throws SQLException {

        insert(auto.getRendszam(), auto.getEvjarat(), auto.getSzin(), auto.getElsotulaj(), auto.getTulaj().getId(), auto.getBerloid().getId(), auto.getOsszeg());
    }

    public void update(String what, String whatvalue, String where, String wherevalue) throws SQLException {

        String sql = "UPDATE " + TABLENAME
                + " set " + what + " = '" + whatvalue + "' WHERE " + where + " = '" + wherevalue + "';";
        try (Statement stmt = mConnection.createStatement()) {
            stmt.executeUpdate(sql);

            printTable();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        }
    }

    public void delete(String where, String wherevalue, char symbol) throws SQLException {
        String sql = "DELETE from " + TABLENAME
                + " WHERE " + where + symbol + " '" + wherevalue + "';";
        try (Statement stmt = mConnection.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        }
        printTable();

    }


}

package hu.carrenter.db.dao;


import hu.carrenter.db.DbConnect;
import hu.carrenter.db.entity.Tulajdonos;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * TODO: Legyen egy AllTest osztály, amelyet megfuttatva megfut az összes teszt -> SuiteClasses
 * @author Barnabas Gnam
 * @since 2017-07-03
 */
public class DaoTulajdonosTest {
    private static final Logger logger = Logger.getLogger(DaoTulajdonosTest.class.getName());
    static Tulajdonos alcapone = new Tulajdonos(1, "Al", "Capone", 0630, "sebhelyes@arcu.hu", "nowhere", 0);

    @Test
    public void daoTulajdonosGetByTest() {
        new DbConnect() {
            @Override
            public void execute(Connection connection) throws SQLException {
                DaoTulajdonos daoTulajdonos = new DaoTulajdonos(connection);
                daoTulajdonos.createTable();
                daoTulajdonos.insert(alcapone);
                Assert.assertEquals("hibas tulaj", "Capone", daoTulajdonos.getById(1).getLastname());
            }
        };
    }
}

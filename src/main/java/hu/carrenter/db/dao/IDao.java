package hu.carrenter.db.dao;


import java.sql.SQLException;

/**
 * @author Barnabas Gnam
 * @since 2017-06-29
 */
interface IDao {


    void createTable() throws SQLException;

}

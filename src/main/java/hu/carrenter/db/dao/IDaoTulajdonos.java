package hu.carrenter.db.dao;

import hu.carrenter.db.entity.Tulajdonos;

import java.sql.SQLException;

/**
 * @author Barnabas Gnam
 * @since 2017-06-29
 */
public interface IDaoTulajdonos extends IDao {
    void insert(int id, String firstname, String lastname, int phone, String email, String address, int rating) throws SQLException;

    void insert(Tulajdonos tulaj) throws SQLException;

    Tulajdonos getById(int id) throws SQLException;


}

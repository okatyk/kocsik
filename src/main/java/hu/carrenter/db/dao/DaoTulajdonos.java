package hu.carrenter.db.dao;

import hu.carrenter.db.entity.Tulajdonos;

import java.sql.*;
import java.util.logging.Logger;

/**
 * @author Barnabas Gnam
 * @since 2017-06-28
 */
public class DaoTulajdonos extends DaoCarRenter implements IDaoTulajdonos {
    private static final Logger logger = Logger.getLogger(DaoTulajdonos.class.getName());

    public static final String TABLENAME = "owners";
    private static final String ID = "id";
    private static final String FIRSTNAME = "firstname";
    private static final String LASTNAME = "lastname";
    private static final String PHONE = "phonenumber";
    private static final String EMAIL = "emailaddress";
    private static final String ADDRESS = "address";
    private static final String RATING = "rating";



    public DaoTulajdonos(Connection connection) throws SQLException {
        mConnection = connection;
    }

    public void createTable() throws SQLException {

        try (Statement stmt = mConnection.createStatement()) {
            String drop = "DROP TABLE IF EXISTS " + TABLENAME + ";";

            stmt.executeUpdate(drop);

            String sql = "CREATE TABLE " + TABLENAME + ""
                    + " " + "(" + ID + " integer not NULL primary key, "
                    + " " + FIRSTNAME + " VARCHAR (255) not NULL, "
                    + " " + LASTNAME + " varchar(255), "
                    + " " + PHONE + " INTEGER, "
                    + " " + EMAIL + " varchar(255), "
                    + " " + ADDRESS + " varchar(255), "
                    + " " + RATING + " INTEGER ) "
                    + ";";
            stmt.executeUpdate(sql);
            logger.info(TABLENAME + " tabla letrehozva");

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public void insert(int id, String firstname, String lastname, int phone, String email, String address, int rating) throws SQLException {
        String sql = "INSERT INTO " + TABLENAME + " VALUES (?,?,?,?,?,?,?);";
        try (PreparedStatement stmt = mConnection.prepareStatement(sql)) {
            stmt.setInt(1, id);
            stmt.setString(2, firstname);
            stmt.setString(3, lastname);
            stmt.setInt(4, phone);
            stmt.setString(5, email);
            stmt.setString(6, address);
            stmt.setInt(7, rating);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }


    }

    public void insert(Tulajdonos tulaj) throws SQLException {
        insert(tulaj.getId(), tulaj.getFirstname(), tulaj.getLastname(), tulaj.getPhonenumber(), tulaj.getEmail(), tulaj.getAddress(), tulaj.getRating());
        logger.info("tulaj insert kesz");
    }

    public Tulajdonos getById(int id) throws SQLException {
        Tulajdonos uj = null;

        try (Statement stmt = mConnection.createStatement()) {
            // TODO: Result setet lezárni majd
            ResultSet resultSet = stmt.executeQuery("SELECT * from " + TABLENAME + " WHERE id = '" + id + "';");
            resultSet.next();
            logger.info(resultSet.getString(2));
            uj = new Tulajdonos(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getInt(4), resultSet.getString(5), resultSet.getString(6), resultSet.getInt(7));
            logger.info("A tulajdonos : " + uj.getFirstname() + " " + uj.getLastname());

            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        return uj;

    }

}

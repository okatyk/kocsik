package hu.carrenter.db.dao;

import hu.carrenter.db.DbConnect;


import hu.carrenter.db.entity.Berlo;
import hu.carrenter.db.entity.Cars;
import hu.carrenter.db.entity.Tulajdonos;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;



/**
 * @author Barnabas Gnam
 * @since 2017-07-03
 */

public class DaoCarsTest {
    // TODO Láthatóságokat miért nem szoktad beállítani?
   private static Cars mustang;
   private static Cars lada;
   private static Tulajdonos alcapone;
    private static final Logger logger = Logger.getLogger(DaoCarsTest.class.getName());

    @BeforeClass
    public static void BeforeClassTest() {

        alcapone = new Tulajdonos(1, "Al", "Capone", 0630, "sebhelyes@arcu.hu", "nowhere", 0);
        Berlo rent = new Berlo(2, "Rent", "Er", 2222, "ren@ter.hu", "renhouse", 13213211);
   //     mustang = new Cars("bos-002", 1967, "fekete", false, alcapone, rent, 123);
   //     lada = new Cars("lad-111", 1988, "piros", false, alcapone, rent, 143);
    }


    @Test
    public void daoCarsInsertTest() {
        logger.info("TEST : testInsert");
        new DbConnect() {
            @Override
            public void execute(Connection connection) throws SQLException {
                DaoCars daoCars = new DaoCars(connection);

                daoCars.createTable();
          //      daoCars.insert(mustang);
        //        daoCars.insert(lada);
        //        List<Cars> autok = daoCars.getAll();
       //         Assert.assertEquals("piros", autok.get(1).getSzin());


            }
        };
    }

    @Test
    public void daoCarsGetByTest() {
        logger.info("TEST : testgetByREndszamTest");

        new DbConnect() {
            @Override
            public void execute(Connection connection) throws SQLException {
                DaoCars daoCars = new DaoCars(connection);
                   daoCars.createTable();
          //           daoCars.insert(mustang);
        //        Assert.assertEquals("Hibas a tulaj ID", alcapone.getId(), daoCars.getByRendszam("bos-002").getTulaj().getId());
            }
        };

    }

    @Test
    public void daoCarsUpdateTest() {
        logger.info("TEST : updateTest");

        new DbConnect() {
            @Override
            public void execute(Connection connection) throws SQLException {
                DaoCars daoCars = new DaoCars(connection);
                daoCars.printTable();
                daoCars.update(DaoCars.RENDSZAM, "bos-003", DaoCars.RENDSZAM, "bos-002");

                daoCars.printTable();

            }

        };
    }

    @Test
    public void daoCarsDeleteTest() {
        new DbConnect() {
            @Override
            public void execute(Connection connection) throws SQLException {
                DaoCars daoCars = new DaoCars(connection);
                daoCars.delete(DaoCars.EVJARAT, "1967", '=');
                daoCars.printTable();

            }
        };
    }
}
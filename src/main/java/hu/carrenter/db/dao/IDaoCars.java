package hu.carrenter.db.dao;

import hu.carrenter.db.entity.Cars;

import java.sql.SQLException;

/**
 * @author Barnabas Gnam
 * @since 2017-06-29
 */
public interface IDaoCars extends IDao {
    void insert(String rendszam, int evjarat, String szin, Boolean elsotulaj, int tulaj, int berlo, int osszeg) throws SQLException;

    void insert(Cars auto) throws SQLException;

    void update(String what, String whatvalue, String where, String wherevalue) throws SQLException;

 //   Cars getByRendszam(String rendszam) throws SQLException;

 //   List<Cars> getAll() throws SQLException;

    void printTable() throws SQLException;

    void delete(String where, String wherevalue, char symbol) throws SQLException;
}


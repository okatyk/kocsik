package hu.carrenter.db.dao;

import hu.carrenter.db.DbConnect;
import hu.carrenter.db.entity.Berlo;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * TODO: A testsz osztályok ugyanolyan package alatt legyenek, mint amit tesztelünk
 * @author Barnabas Gnam
 * @since 2017-07-03
 */
public class DaoBerloTest {
    private static final Logger logger = Logger.getLogger(DaoBerloTest.class.getName());
    Berlo rent = new Berlo(2, "Rent", "Er", 2222, "ren@ter.hu", "renhouse", 13213211);

    @Test
    public void daoBerlogetByTest() {
        logger.info("TEST : DAOBERLO");
        new DbConnect() {
            @Override
            public void execute(Connection connection) throws SQLException {
                DaoBerlo daoBerlo = new DaoBerlo(connection);
                daoBerlo.createTable();
                daoBerlo.insert(rent);
                Assert.assertEquals("hibas a berlo", "Rent", daoBerlo.getById(2).getFirstname());
            }
        };

    }
}

package hu.carrenter.db.dao;

import hu.carrenter.db.entity.Berlo;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;


import java.sql.*;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Barnabas Gnam
 * @since 2017-06-28
 */
public class DaoBerlo extends DaoCarRenter implements IDaoBerlo {

    public static final String TABLENAME = "renter";
    private static final String ID = "id";
    private static final String FIRSTNAME = "firstname";
    private static final String PHONE = "phonenumber";
    private static final String LASTNAME = "lastname";
    private static final String EMAIL = "emailaddress";
    private static final String ADDRESS = "address";
    private static final String KERET = "keret";

    private static final Logger logger = Logger.getLogger(DaoBerlo.class.getName());


    public DaoBerlo(Connection connection) throws SQLException {
        mConnection = connection;

    }

    public void createTable() throws SQLException {
        String drop = "DROP TABLE IF EXISTS " + TABLENAME + ";";
        String sql = "CREATE TABLE " + TABLENAME + ""
                + " " + "(" + ID + " integer not NULL primary key, "
                + " " + FIRSTNAME + " VARCHAR (255) not NULL, "
                + " " + LASTNAME + " varchar(255), "
                + " " + PHONE + " INTEGER, "
                + " " + EMAIL + " varchar(255), "
                + " " + ADDRESS + " varchar(255), "
                + " " + KERET + " INTEGER ) "
                + ";";


        try (Statement stmt = mConnection.createStatement()) {


            stmt.executeUpdate(drop);


            stmt.executeUpdate(sql);
            logger.info(TABLENAME + " tabla letrehozva");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }


    public void insert(int id, String firstname, String lastname, int phone, String email, String address, int keret) throws SQLException {

        String sql = "INSERT INTO " + TABLENAME + " VALUES (?,?,?,?,?,?,?);";
        try (PreparedStatement stmt = mConnection.prepareStatement(sql)) {
            stmt.setInt(1, id);
            stmt.setString(2, firstname);
            stmt.setString(3, lastname);
            stmt.setInt(4, phone);
            stmt.setString(5, email);
            stmt.setString(6, address);
            stmt.setInt(7, keret);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


    public void insert(Berlo berlo) throws SQLException {
        insert(berlo.getId(), berlo.getFirstname(), berlo.getLastname(), berlo.getPhonenumber(), berlo.getEmail(), berlo.getAddress(), berlo.getKeret());
        logger.info("berlo insert kesz");
    }

    public Berlo getById(int id) throws SQLException {
        Berlo uj = null;
        try (Statement stmt = mConnection.createStatement()) {
            ResultSet resultSet = stmt.executeQuery("SELECT * from " + TABLENAME + " WHERE id =" + id + ";");
            resultSet.next();
            uj = new Berlo(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getInt(4), resultSet.getString(5), resultSet.getString(6), resultSet.getInt(7));
            resultSet.close();
            logger.info("A berlo : " + uj.getFirstname() + " " + uj.getLastname());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return uj;

    }
/*
    public Berlo getByIdCrit(Session session, int id) throws SQLException {
        Criteria criteria = session.createCriteria(Berlo.class);
        criteria.add(Restrictions.eq("id", 2));
        List<Berlo> list = criteria.list();

    }*/

}

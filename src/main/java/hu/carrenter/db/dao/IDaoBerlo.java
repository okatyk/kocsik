package hu.carrenter.db.dao;

import hu.carrenter.db.entity.Berlo;

import java.sql.SQLException;

/**
 * @author Barnabas Gnam
 * @since 2017-06-29
 */
public interface IDaoBerlo extends IDao {
    void insert(int id, String firstname, String lastname, int phone, String email, String address, int keret) throws SQLException;

    void insert(Berlo berlo) throws SQLException;

    Berlo getById(int id) throws SQLException;
}

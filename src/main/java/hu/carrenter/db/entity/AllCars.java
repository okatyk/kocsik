package hu.carrenter.db.entity;

import hu.carrenter.db.hib.HibCars;
import org.hibernate.Session;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;


/**
 * @author Barnabas Gnam
 * @since 2017-07-18
 */
@XmlRootElement(name = "Allcars")
public class AllCars {
    private List<Cars> osszes = null;
    private static final Logger logger = Logger.getLogger(AllCars.class.getName());

    public AllCars() {
        osszes = new ArrayList<Cars>();
    }

    public AllCars(Session session) {
        HibCars hibCars = new HibCars(session);
        osszes = hibCars.getAll();
    }

    @XmlElement(name = "cars")
    public List<Cars> getOsszes() {
        return osszes;
    }

    public void setOsszes(List<Cars> kocsi) {
        this.osszes = kocsi;
    }

    public void exportXML(File file) {
        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(AllCars.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(this, System.out);


            jaxbMarshaller.marshal(this, file);
        } catch (JAXBException j) {
            j.printStackTrace();
        }

    }

    public void importXML(String file) throws FileNotFoundException {
       if(validateXML(file)){
           logger.info("xml validate done!");
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(AllCars.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            AllCars uj = (AllCars) unmarshaller.unmarshal(new File(getResource(file)));

            this.setOsszes(uj.getOsszes());

        } catch (JAXBException j) {
            j.printStackTrace();
        }}
        else logger.warning("xml validate error");

    }

    public Boolean validateXML(String xml) {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        try {
            Schema schema = factory.newSchema((new File(getResource("cars.xsd"))));

            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(getResource("cars.xml"))));
            return true;
        } catch (SAXException | IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    private String getResource(String filename) throws FileNotFoundException {
        URL resource = getClass().getClassLoader().getResource(filename);
        Objects.requireNonNull(resource);

        return resource.getFile();
    }
}
package hu.carrenter.db.entity;

/**
 * @author Barnabas Gnam
 * @since 2017-06-30
 */
interface ISzemely {

    int getId();

    String getFirstname();

    String getLastname();

    int getPhonenumber();
    String getEmail();

    String getAddress();
}

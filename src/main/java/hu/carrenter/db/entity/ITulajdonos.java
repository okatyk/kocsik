package hu.carrenter.db.entity;

/**
 * @author Barnabas Gnam
 * @since 2017-06-30
 */
interface ITulajdonos extends ISzemely {
    int getRating();
}

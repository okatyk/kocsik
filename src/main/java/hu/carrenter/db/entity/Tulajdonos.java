package hu.carrenter.db.entity;

import hu.carrenter.db.dao.DaoTulajdonos;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Barnabas Gnam
 * @since 2017-06-27
 */
@XmlType(name = "tulaj", propOrder = {
        "id",
        "firstname",
        "lastname",
        "address",
        "email",
        "phonenumber",
        "rating"
})
@Entity
@Table(name = DaoTulajdonos.TABLENAME)
public class Tulajdonos extends Szemely implements ITulajdonos {
    @Column(name = "rating")
    private int rating;
    //@ElementCollection
    //@CollectionTable(name = "cars")
 //   @JoinColumn
  //  @OneToMany(mappedBy = "tulaj")
 //   private Set<Cars> ownedcars;
    public Tulajdonos() {
    }


    public Tulajdonos(int id, String firstname, String lastname, int phonenumber, String email, String address, int rating) {
        super(id, firstname, lastname, phonenumber, email, address);
        this.rating = rating;
        //ownedcars = new HashSet<Cars>();
    }

    public Tulajdonos(Tulajdonos regi) {
        super(regi.getId(), regi.getFirstname(), regi.getLastname(), regi.getPhonenumber(), regi.getEmail(), regi.getAddress());
        this.rating = regi.getRating();
    }

    public int getRating() {
        return rating;

    }

    public void setRating(int rating) {
        this.rating = rating;
    }

  //  public Set<Cars> getOwnedcars() {
 //       return ownedcars;
 //   }

 //   public void setOwnedcars(Set<Cars> ownedcars) {
 //       this.ownedcars = ownedcars;
 //   }
  //  public void addOwnedcars(Cars kocsi){
  //      ownedcars.add(kocsi);
///

    //}
}

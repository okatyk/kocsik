package hu.carrenter.db.entity;


import com.sun.jndi.ldap.Ber;
import hu.carrenter.db.dao.DaoCars;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.logging.Logger;

import static hu.carrenter.db.dao.DaoCars.TULAJID;


/**
 * @author Barnabas Gnam
 * @since 2017-06-26
 */
@Entity
@Table(name = "cars")
@XmlRootElement(name = "cars")
@XmlAccessorType(XmlAccessType.NONE)
@XmlType(propOrder ={ "rendszam","szin","elsotulaj","tulaj","berloid"})

public class Cars implements ICars {
    private static final Logger logger = Logger.getLogger(Cars.class.getName());
    //   private int id;
    @Id
    @Column(name = DaoCars.RENDSZAM)
    private String rendszam;
    @Column(name = DaoCars.EVJARAT)
    private int evjarat;
    @Column(name = DaoCars.SZIN)
    private String szin;
    @Column(name = DaoCars.ELSOTULAJ)
    private Boolean elsotulaj;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = TULAJID)
    private Tulajdonos tulaj;
    // private Berlo kolcsonzo;
    @Column(name = DaoCars.OSSZEG)
    private int osszeg;
    // @Column(name = TULAJID)
    //  private int tulajid;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = DaoCars.BERLOID)
    private Berlo berloid;

    public Cars() {
    }

    public Cars(String rendszam, int evjarat, String szin, Boolean elsotulaj, Tulajdonos tulaj, Berlo berlo, int osszeg) {
        if (rendszam.length() != 7) {
            logger.warning("hibas rendszam");
            return;
        }
        // if (berlo == null) {
        //   berlo = new Berlo();
        //   }
        //  this.tulajid = tulaj;
        this.berloid = berlo;
        this.rendszam = rendszam;
        this.evjarat = evjarat;
        this.szin = szin;
        this.elsotulaj = elsotulaj;
        this.tulaj = tulaj;
        this.osszeg = osszeg;
        //       this.kolcsonzo = berlo;
       // tulaj.addOwnedcars(this);
   }
    @XmlElement(name = "rendszam")
    public String getRendszam() {
        return rendszam;
    }
    public void setRendszam(String rendszam) {
        this.rendszam = rendszam;
    }
    @XmlElement(name ="szin")
    public String getSzin() {
        return szin;
    }

    public void setSzin(String szin) {
        this.szin = szin;
    }
    @XmlElement(name ="elsotulaj")
    public Boolean getElsotulaj() {
        return elsotulaj;
    }

    public void setElsotulaj(Boolean elsotulaj) {
        this.elsotulaj = elsotulaj;
    }

    public int getEvjarat() {
        return evjarat;
    }
    @XmlElement(name ="Tulajdonos")
    public Tulajdonos getTulaj() {
        return tulaj;
    }

    public void setTulaj(Tulajdonos tulaj) {
        this.tulaj = tulaj;
    }

//    public Berlo getKolcsonzo() {
    //       return kolcsonzo;
    //   }

    //  public void setKolcsonzo(Berlo kolcsonzo) {
    //      this.kolcsonzo = kolcsonzo;
    //   }

    public int getOsszeg() {
        return osszeg;
    }

    public void setOsszeg(int osszeg) {
        this.osszeg = osszeg;
    }


    public void setEvjarat(int evjarat) {
        this.evjarat = evjarat;
    }

    @XmlElement(name = "berlo")
    public Berlo getBerloid() {
        return berloid;
    }

    public void setBerloid(Berlo berloid) {
        this.berloid = berloid;
    }


}

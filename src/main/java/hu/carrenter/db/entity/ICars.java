package hu.carrenter.db.entity;

/**
 * @author Barnabas Gnam
 * @since 2017-07-03
 */
public interface ICars {
    String getRendszam();

    void setRendszam(String rendszam);

    String getSzin();

    void setSzin(String szin);

    Boolean getElsotulaj();

    void setElsotulaj(Boolean elsotulaj);

    int getEvjarat();

//    Tulajdonos getTulaj();

  //  void setTulaj(Tulajdonos tulaj);

   // Berlo getKolcsonzo();

//    void setKolcsonzo(Berlo kolcsonzo);

    int getOsszeg();

    void setOsszeg(int osszeg);




}

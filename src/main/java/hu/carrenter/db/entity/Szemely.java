package hu.carrenter.db.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.*;

/**
 * @author Barnabas Gnam
 * @since 2017-06-27
 */
@MappedSuperclass
@XmlTransient
abstract public class Szemely implements ISzemely {
    @Id
    private int id;
    @Column(name = "firstname")
    private String firstname;
    @Column(name = "lastname")
    private String lastname;
    @Column(name = "phonenumber")
    private int phonenumber;
    @Column(name = "emailaddress")
    private String email;
    @Column(name = "address")
    private String address;

    Szemely() {
    }

    Szemely(int id, String firstname, String lastname, int phonenumber, String email, String address) {
        this.id = id;
        this.firstname = firstname;
       this.lastname = lastname;
        this.phonenumber = phonenumber;
        this.email = email;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public int getPhonenumber() {
        return phonenumber;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setPhonenumber(int phonenumber) {
        this.phonenumber = phonenumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}


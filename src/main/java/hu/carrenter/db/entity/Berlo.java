package hu.carrenter.db.entity;

import hu.carrenter.db.dao.DaoBerlo;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlType;
import java.util.logging.Logger;

/**
 * @author Barnabas Gnam
 * @since 2017-06-27
 */
@XmlType(name = "berlo", propOrder = {
        "id",
        "firstname",
        "lastname",
        "address",
        "email",
        "phonenumber",
        "keret"
})
@Entity
@Table(name = DaoBerlo.TABLENAME)
public class Berlo extends Szemely implements IBerlo {
    private static final Logger logger = Logger.getLogger(Berlo.class.getName());
    @Column(name = "keret")
    private int keret;

    public Berlo(int id, String firstname, String lastname, int phonenumber, String email, String address, int keret) {
        super(id, firstname, lastname, phonenumber, email, address);
        this.keret = keret;
    }

    public Berlo() {
        super(0, "", "", 0, "", "");
        this.keret = 0;
    }

    public int getKeret() {
        return keret;
    }

    public void setKeret(int keret) {
        this.keret = keret;
    }



}

package hu.carrenter.db.entity;

/**
 * @author Barnabas Gnam
 * @since 2017-06-30
 */
interface IBerlo extends ISzemely {
     int getKeret();
     void setKeret(int keret);

}

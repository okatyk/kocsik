package hu.carrenter.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * @author Barnabas Gnam
 * @since 2017-07-06
 */
public abstract class HibConnect {

    public HibConnect() {
        try (SessionFactory factory = new Configuration().configure().buildSessionFactory();
             Session session = factory.openSession()
        ) {

            execute(session);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }


    public abstract void execute(Session session);
}

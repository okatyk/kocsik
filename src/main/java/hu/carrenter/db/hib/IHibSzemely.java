package hu.carrenter.db.hib;

/**
 * @author Barnabas Gnam
 * @since 2017-07-13
 */
public interface IHibSzemely {
void insert(int id, String firstname, String lastname, int phonenumber, String email, String address, int rating);
void deleteById(int id);
}

package hu.carrenter.db.hib;

import hu.carrenter.db.HibConnect;
import hu.carrenter.db.entity.AllCars;
import hu.carrenter.db.entity.Berlo;
import hu.carrenter.db.entity.Cars;
import hu.carrenter.db.entity.Tulajdonos;
import org.hibernate.Session;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Barnabas Gnam
 * @since 2017-07-17
 */
public class HibCarsTest {
    private static final Logger logger = Logger.getLogger(HibCarsTest.class.getName());
    private static Tulajdonos alcapone = new Tulajdonos(1, "Al", "Capone", 0630, "sebhelyes@arcu.hu", "nowhere", 0);
    private static Berlo rent = new Berlo(2, "Rent", "Er", 2222, "ren@ter.hu", "renhouse", 13213211);
    private static Cars lada = new Cars("lad-111", 1988, "piros", false, alcapone, rent, 143);
    private static Cars lada2 = new Cars("lad-112", 1988, "piros", false, alcapone, rent, 143);
    private static Berlo rent2 = new Berlo(3, "Rent", "Er", 2222, "ren@ter.hu", "renhouse", 13213211);
    private static Berlo rent3 = new Berlo(4, "Rent", "Er", 2222, "ren@ter.hu", "renhouse", 13213211);
    private static Cars mustang = new Cars("bos-002", 1967, "fekete", false, alcapone, rent3, 123);

    @BeforeClass
    public static void beforeClassTest() {

        new HibConnect() {
            @Override
            public void execute(Session session) {
                //     session.createNativeQuery("DROP TABLE IF EXIST owners").executeUpdate();
                // session.createNativeQuery("CREATE TABLE CARS").executeUpdate();


                HibTulajdonos hibTulajdonos = new HibTulajdonos(session);
                HibBerlo hibBerlo = new HibBerlo(session);
                HibCars hibCars = new HibCars(session);
                hibBerlo.insert(rent2);
                hibTulajdonos.insert(alcapone);
                hibBerlo.insert(rent);
                hibBerlo.insert(rent3);
                hibCars.insert("new-001", 2017, "feher", true, alcapone, rent, 2000);
                hibCars.insert(mustang);
                hibCars.insert(lada);
                hibCars.insert(lada2);
            }
        };
    }


    @Test
    public void getByTest() {
        new HibConnect() {
            @Override
            public void execute(Session session) {
                List<Cars> lista;

                HibCars hibCars = new HibCars(session);
                lista = hibCars.getAll();
                Assert.assertEquals("hibas", "new-001", lista.get(0).getRendszam());
            }
        };


    }

    @Test
    public void updateTest() {
        new HibConnect() {
            @Override
            public void execute(Session session) {
                HibCars hibCars = new HibCars(session);
                mustang.setSzin("piros");
                hibCars.update(mustang);
                Assert.assertEquals("rossz szin", "piros", hibCars.getByRendszam("bos-002").getSzin());
            }
        };

    }

    @Test
    public void marshalingTest() throws JAXBException {
        new HibConnect() {
            @Override
            public void execute(Session session) {
                AllCars allCars = new AllCars(session);
                File ide = new File("C:\\cygwin64\\home\\carsmavencom\\cars.xml");
                allCars.exportXML(ide);
            }

        };


    }
    @Test public void importTest() throws FileNotFoundException {
        AllCars in = new AllCars();
String xml = "cars.xml";
        in.importXML(xml);
        logger.info(String.valueOf(in.getOsszes().size()));

    }
    @Test public void validateTest(){
        AllCars allCars = new AllCars();
        String xml = "cars.xml";
        allCars.validateXML(xml);

    }
}

package hu.carrenter.db.hib;

import hu.carrenter.db.entity.Berlo;
import hu.carrenter.db.entity.Cars;
import hu.carrenter.db.entity.Tulajdonos;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Barnabas Gnam
 * @since 2017-07-12
 */
public class HibCars extends HibCarRenter implements IHibCars {
    private static final Logger logger = Logger.getLogger(HibCars.class.getName());

    public HibCars(Session session) {
        msession = session;
    }

    public void insert(String rendszam, int evjarat, String szin, Boolean elsotulaj, Tulajdonos tulaj, Berlo berlo, int osszeg) {
        Transaction tx = null;
        try {

            if (msession.get(Cars.class, rendszam) == null) {
                tx = msession.beginTransaction();
                Cars carId1 = new Cars();
                carId1.setRendszam(rendszam);
                carId1.setEvjarat(evjarat);
                carId1.setSzin(szin);
                carId1.setElsotulaj(elsotulaj);
                carId1.setTulaj(tulaj);
                carId1.setBerloid(berlo);
                carId1.setOsszeg(osszeg);
                msession.save(carId1);
                tx.commit();
                logger.info(rendszam + " INSERT DONE!");
            } else
                logger.warning("létezik már : " + rendszam);
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }

    }


    public void insert(Cars cars) {
        insert(cars.getRendszam(), cars.getEvjarat(), cars.getSzin(), cars.getElsotulaj(), cars.getTulaj(), cars.getBerloid(), cars.getOsszeg());

    }
        @XmlElement(name="cars")
    public List<Cars> getAll() {
        CriteriaBuilder builder = msession.getCriteriaBuilder();

        CriteriaQuery<Cars> criteriaQuery = builder.createQuery(Cars.class);
        Root<Cars> critcar = criteriaQuery.from(Cars.class);
        criteriaQuery.select(critcar);

        TypedQuery<Cars> q = msession.createQuery(criteriaQuery);


        return q.getResultList();
    }

    public Cars getByRendszam(String rendszam) {
        CriteriaBuilder builder = msession.getCriteriaBuilder();

        CriteriaQuery<Cars> criteriaQuery = builder.createQuery(Cars.class);
        Root<Cars> critcar = criteriaQuery.from(Cars.class);
        criteriaQuery.select(critcar).where(builder.equal(critcar.get("rendszam"), rendszam));

        TypedQuery<Cars> q = msession.createQuery(criteriaQuery);

        Cars uj = q.getSingleResult();
        return uj;

    }

    public void update(Cars cars) {

        Transaction tx = null;
        try {
            tx = msession.beginTransaction();

            msession.update(cars);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
    }

    public void delete(String rendszam) {
        Transaction tx = null;
        try {
            tx = msession.beginTransaction();
            Cars cars = msession.load(Cars.class, rendszam);
            msession.delete(cars);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
    }

}

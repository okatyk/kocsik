package hu.carrenter.db.hib;


import hu.carrenter.db.HibConnect;
import hu.carrenter.db.entity.Berlo;
import hu.carrenter.db.entity.Cars;
import hu.carrenter.db.entity.Tulajdonos;
import org.hibernate.Session;

import java.util.List;
import java.util.logging.Logger;

/**
 * @author Barnabas Gnam
 * @since 2017-07-04
 */
public class HibTest {
    private static final Logger logger = Logger.getLogger(HibTest.class.getName());

    public static void main(String[] args) {
        Tulajdonos alcapone = new Tulajdonos(1, "Al", "Capone", 0630, "sebhelyes@arcu.hu", "nowhere", 0);
        Berlo rent = new Berlo(2, "Rent", "Er", 2222, "ren@ter.hu", "renhouse", 13213211);
        Berlo rent2 = new Berlo(3, "Rent", "Er", 2222, "ren@ter.hu", "renhouse", 13213211);
        Berlo rent3 = new Berlo(4, "Rent", "Er", 2222, "ren@ter.hu", "renhouse", 13213211);


        new HibConnect() {
            @Override
            public void execute(Session session) {
                HibCars hibCars = new HibCars(session);
                HibTulajdonos hibTulajdonos = new HibTulajdonos(session);
                HibBerlo hibBerlo = new HibBerlo(session);
                Tulajdonos alcapone = new Tulajdonos(1, "Al", "Capone", 0630, "sebhelyes@arcu.hu", "nowhere", 0);

                hibBerlo.insert(rent);
                hibBerlo.insert(rent2);
                hibBerlo.insert(rent3);
                hibTulajdonos.insert(alcapone);
                hibCars.insert("hib-341", 2017, "kek", false, alcapone, rent2, 123);
                Cars mustang = new Cars("bos-002", 1967, "fekete", false, alcapone, rent3, 123);
                Cars lada = new Cars("lad-111", 1988, "piros", false, alcapone, rent, 143);
                Cars lada2 = new Cars("lad-112", 1988, "piros", false, alcapone, rent, 143);
                hibCars.insert(mustang);
                hibCars.insert(lada);
                hibCars.insert(lada2);
                hibCars.delete("lad-112");
// update
                Cars updateable = hibCars.getByRendszam("bos-002");
                updateable.setSzin("szurke");
                hibCars.update(updateable);
                List<Cars> all = hibCars.getAll();
                logger.info("az osszes auto rendszama : ");
                for (int i = 0; i < all.size(); i++) {
                    logger.info(all.get(i).getRendszam());
                }
            Berlo test = hibBerlo.getById(3);
                logger.info(test.getFirstname());
            }
        };

    }


}



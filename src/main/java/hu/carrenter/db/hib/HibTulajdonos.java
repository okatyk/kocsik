package hu.carrenter.db.hib;

import hu.carrenter.db.entity.Tulajdonos;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Barnabas Gnam
 * @since 2017-07-13
 */
public class HibTulajdonos extends HibCarRenter implements IHibTulajdonos {
    private static final Logger logger = Logger.getLogger(HibTulajdonos.class.getName());

    public HibTulajdonos(Session session) {
        msession = session;
    }

    public void insert(int id, String firstname, String lastname, int phonenumber, String email, String address, int rating) {
        Transaction tx = null;

        try {
            tx = msession.beginTransaction();
            Tulajdonos tulajdonos = new Tulajdonos(id, firstname, lastname, phonenumber, email, address, rating);
            if (msession.get(Tulajdonos.class, id) == null)
                msession.save(tulajdonos);
            else {
                logger.warning("létezik már :"+firstname+" "+lastname);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
    }

    public void insert(Tulajdonos tulajdonos) {
        insert(tulajdonos.getId(), tulajdonos.getFirstname(), tulajdonos.getLastname(), tulajdonos.getPhonenumber(), tulajdonos.getEmail(), tulajdonos.getAddress(), tulajdonos.getRating());
    }

    public void update(Tulajdonos tulajdonos) {
        Transaction tx = null;
        try {
            tx = msession.beginTransaction();
            msession.update(tulajdonos);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
    }

    public List<Tulajdonos> getAll() {
        CriteriaBuilder builder = msession.getCriteriaBuilder();

        CriteriaQuery<Tulajdonos> criteriaQuery = builder.createQuery(Tulajdonos.class);
        Root<Tulajdonos> critcar = criteriaQuery.from(Tulajdonos.class);
        criteriaQuery.select(critcar);

        TypedQuery<Tulajdonos> q = msession.createQuery(criteriaQuery);


        return q.getResultList();
    }

    public Tulajdonos getById(int id) {
        CriteriaBuilder builder = msession.getCriteriaBuilder();

        CriteriaQuery<Tulajdonos> criteriaQuery = builder.createQuery(Tulajdonos.class);
        Root<Tulajdonos> critcar = criteriaQuery.from(Tulajdonos.class);
        criteriaQuery.select(critcar).where(builder.equal(critcar.get("id"), id));

        TypedQuery<Tulajdonos> q = msession.createQuery(criteriaQuery);

        Tulajdonos uj = q.getSingleResult();
        return uj;
    }

    public void deleteById(int id) {
        Tulajdonos temp = getById(id);
        Transaction tx = null;
        try {
            tx = msession.beginTransaction();
            msession.delete(temp);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();

        }
    }

}


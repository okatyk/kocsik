package hu.carrenter.db.hib;

import hu.carrenter.db.entity.Berlo;
import hu.carrenter.db.entity.Cars;
import hu.carrenter.db.entity.Tulajdonos;

import java.util.List;

/**
 * @author Barnabas Gnam
 * @since 2017-07-13
 */
public interface IHibCars {


    void insert(String rendszam, int evjarat, String szin, Boolean elsotulaj, Tulajdonos tulaj, Berlo berlo, int osszeg);


    void insert(Cars cars);

    List<Cars> getAll();

    Cars getByRendszam(String rendszam);

    void update(Cars cars);

    void delete(String rendszam);

}

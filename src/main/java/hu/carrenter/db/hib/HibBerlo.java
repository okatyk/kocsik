package hu.carrenter.db.hib;

import hu.carrenter.db.entity.Berlo;
import hu.carrenter.db.entity.Cars;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Barnabas Gnam
 * @since 2017-07-13
 */
public class HibBerlo extends HibCarRenter implements IHibBerlo {
    private static final Logger logger = Logger.getLogger(HibBerlo.class.getName());

    public HibBerlo(Session session) {
        msession = session;
    }

    public void insert(int id, String firstname, String lastname, int phonenumber, String email, String address, int keret) {
        Transaction tx = null;
        try {
            tx = msession.beginTransaction();
            Berlo berlo = new Berlo(id, firstname, lastname, phonenumber, email, address, keret);

            if (msession.get(Berlo.class, id) == null)
                msession.save(berlo);
            else {
                logger.warning("létezik már :" + firstname + " " + lastname);
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }

    }

    public void insert(Berlo berlo) {
        insert(berlo.getId(), berlo.getFirstname(), berlo.getLastname(), berlo.getPhonenumber(), berlo.getEmail(), berlo.getAddress(), berlo.getKeret());

    }

    public List<Berlo> getAll() {
        CriteriaBuilder builder = msession.getCriteriaBuilder();

        CriteriaQuery<Berlo> criteriaQuery = builder.createQuery(Berlo.class);
        Root<Berlo> critcar = criteriaQuery.from(Berlo.class);
        criteriaQuery.select(critcar);

        TypedQuery<Berlo> q = msession.createQuery(criteriaQuery);


        return q.getResultList();
    }

    public Berlo getById(int id) {
        CriteriaBuilder builder = msession.getCriteriaBuilder();

        CriteriaQuery<Berlo> criteriaQuery = builder.createQuery(Berlo.class);
        Root<Berlo> critcar = criteriaQuery.from(Berlo.class);
        criteriaQuery.select(critcar).where(builder.equal(critcar.get("id"), id));

        TypedQuery<Berlo> q = msession.createQuery(criteriaQuery);

        Berlo uj = q.getSingleResult();
        return uj;

    }

    public void update(Berlo berlo) {
        Transaction tx = null;
        try {
            tx = msession.beginTransaction();
            msession.update(berlo);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }

    }

    public void deleteById(int id) {
        Berlo temp = getById(id);
        Transaction tx = null;
        try {
            tx = msession.beginTransaction();
            msession.delete(temp);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();

        }
    }
}

package hu.carrenter.db.hib;

import hu.carrenter.db.entity.Tulajdonos;

/**
 * @author Barnabas Gnam
 * @since 2017-07-13
 */
public interface IHibTulajdonos extends IHibSzemely {

    void insert(Tulajdonos tulajdonos);
    void update(Tulajdonos tulajdonos);
    Tulajdonos getById(int id);
}

package hu.carrenter.db.hib;

import hu.carrenter.db.entity.Berlo;

/**
 * @author Barnabas Gnam
 * @since 2017-07-13
 */
public interface IHibBerlo extends IHibSzemely {
    void insert(Berlo berlo);
    void update(Berlo berlo);
   Berlo getById(int id);
}
